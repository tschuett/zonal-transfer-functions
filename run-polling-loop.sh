#! /bin/bash
# Script to compute net transfer along theta for various times

# NB: Run from /scratch

# Set constants
shared_dir="${HOME}/Work/zonal-flow-drive"
times_file="$shared_dir/times.txt"
lock_file="$shared_dir/lock"
log_file="$shared_dir/transfer.log"
touch $log_file
chmod 644 $log_file
# input file must be in /scratch, not $shared_dir, or it won't run properly
# input file can't be in $shared_dir anyway as the file is so large that it fills my $HOME quota
# However, input file can just be a link from /scratch to hwdisks
input_file="input.out.nc"
ln -sf ${HOME}/plasmahwdisks_users/backup/phd/current/zonal-flow-drive/13-perturbation/re-do-with-write_x_over_time/tprim_1.6/initial/$input_file
host=$(cu | tail -n 1 | awk '{print $1}')

# Define functions
is_finished()
{
    if [ -s $times_file ]
    then
        echo "False"
    else
        echo "True"
    fi
}


echo_to_log()
{
    echo $1  # to stdout
    echo "$(date '+%x %X') - $host: $1" >> $log_file
}


# Loop while there is still work to be done
while [ $(is_finished) == "False" ]
do
    # Check for lock file
    for i in {1..10}
    do
        if (( $i == 10 ))
        then
            echo_to_log "Max. waits reached. Exiting..."
            exit
        fi
        if [ ! -e $lock_file ]
        then
            break
        fi
        echo_to_log "Lock file present. Waiting... (wait number $i)"
        sleep 1
    done
    # Work out available cores
    free_phys=$(cu | tail -n 1 | awk '{print $9}')
    if [ $(python3 -c "print($free_phys < 1)") == "True" ]
    then
        echo_to_log "No cores available. Exiting..."
        exit
    fi
    nproc=$(python3 -c "print(min(int($free_phys), 8))")
    # Lock, get next job, unlock
    touch $lock_file
    t=$(head -n 1 $times_file)
    sed -i "1d" $times_file
    rm $lock_file
    # Run job
    echo_to_log "Calculating T(theta) at timestep number $t using $nproc cores (currently $free_phys free)..."
    out_file="$shared_dir/T_theta_$t.txt"
    SECONDS=0
    $shared_dir/compute_transfer_function.py $input_file $t $(calc $t+1) $nproc $out_file
    chmod 644 $out_file
    echo_to_log "Completed calculating T(theta) at timestep number $t using $nproc cores. Time taken: $(seconds_to_time $SECONDS)"
    echo ""  # stdout only
done
