#! /usr/bin/env python3
"""
Script to compute nonlinear transfer function

Copyright 2020 Steve Biggs

This file is part of initial.

initial is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

initial is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with initial.  If not, see http://www.gnu.org/licenses/.
"""


import netCDF4 as nc
import multiprocessing as mp
import numpy as np
import os
import sys
import xarray as xr


def parse_args():
    input_filename, it_start, it_end, nproc, output_filename, tf_type = sys.argv[1:]
    it_start = int(it_start)
    it_end = int(it_end)
    nproc = int(nproc)
    return input_filename, it_start, it_end, nproc, output_filename, tf_type


def parallel_compute_transfer():
    # Loop over time in parallel
    with mp.Pool(processes=nproc) as p:
        results = np.array(
            p.map(parallel_compute_transfer_wrapper, range(nt), nt // nproc),
            dtype=float,
        )
    return results


def parallel_compute_transfer_wrapper(it):
    print("Computing transfer for t = {}".format(t[it]))
    da_name = "ntot_t" if tf_type == "n" else "density_t"
    if is_Tv:
        T = compute_T_v_this_t(
            ds["phi_t"][it, :, :, itheta0, :],  # phi_t(t, ky, kx, theta, ri)
        )
    else:
        T = compute_T_n_this_t(
            ds[da_name][
                it,
                0,  # 0 here because n_tot is per species but we only have one
                :,
                :,
                itheta0,
                :,
            ],  # ntot_t(t, species, ky, kx, theta, ri)
            ds["phi_t"][it, :, :, itheta0, :],  # phi_t(t, ky, kx, theta, ri)
        )
    return T


def compute_T_v_this_t(phi):
    # Convert ri dimension to complex dtype
    phi = ri_to_complex(phi)
    # Make full versions of arrays to simplify indexing
    phi = make_full(phi)
    # Pre-shift arrays so that indexing works correctly
    phi = np.fft.fftshift(phi)
    # Pre-compute complex conjugate array
    phi_star = np.conj(phi)
    # Pre-prepare array of mediators for performance
    phi_m = compute_phi_m(phi, ikx0, iky0)
    # Compute transfer and return
    return compute_transfer(phi, phi_star, phi_m)


def compute_T_n_this_t(ntot, phi):
    # Convert ri dimension to complex dtype
    ntot = ri_to_complex(ntot)
    phi = ri_to_complex(phi)
    # Make full versions of arrays to simplify indexing
    ntot = make_full(ntot)
    phi = make_full(phi)
    # Pre-shift arrays so that indexing works correctly
    ntot = np.fft.fftshift(ntot)
    phi = np.fft.fftshift(phi)
    # Pre-compute complex conjugate array
    ntot_star = np.conj(ntot)
    # Pre-prepare array of mediators for performance
    phi_m = compute_phi_m(phi, ikx0, iky0)
    # Compute transfer and return
    return compute_transfer(ntot, ntot_star, phi_m)


def ri_to_complex(array_ri):
    array_complex = np.zeros(array_ri.shape[:-1], dtype=complex)
    array_complex.real = array_ri[..., 0]
    array_complex.imag = array_ri[..., 1]
    return array_complex


def make_full(array):
    # Assumes dimension order is ky, kx
    # For concatenated array:
    # - ky slice = -1:0:-1 so that we include non-zero kys in reverse order
    # - kx slice = -1::-1 so that we include all kxs in reverse order but this puts kx = 0
    #     to the end even though we still want it at the start, hence use of
    #     np.roll(..., 1, axis=1) so that kx = 0 is at ikx = 0
    # - np.conj(...) of the reversed and rolled array as the negative kys are the conjugate
    #     of the positive kys
    # - np.concatenate(..., axis=0) so that we concatenate in the ky direction
    return np.concatenate(
        (array, np.conj(np.roll(array[-1:0:-1, -1::-1], 1, axis=1))),
        axis=0,
    )


def compute_phi_m(phi, ikx0, iky0):
    # Pre-allocate array
    # NB: Cannot pre-allocate this variable in the global scope and re-use it here
    phi_m = np.zeros((nky, nkx, nky, nkx), dtype=complex)
    # Loop over target and source wavenumbers
    for ikys in range(nky):
        for ikxs in range(nkx):
            for ikyt in range(nky):
                # Work out index of mediator
                ikym = ikyt - ikys + iky0
                # Check mediator index exists
                if not (0 <= ikym and ikym < nky):
                    # Just don't set a value to avoid unnecessary cache misses
                    continue
                for ikxt in range(nkx):
                    # Work out index of mediator
                    ikxm = ikxt - ikxs + ikx0
                    # Check mediator index exists
                    if not (0 <= ikxm and ikxm < nkx):
                        # Just don't set a value to avoid unnecessary cache misses
                        continue
                    # Store mediator value in mediator array
                    phi_m[ikys, ikxs, ikyt, ikxt] = phi[ikym, ikxm]
    # Return output array
    return phi_m


def compute_transfer(f, f_star, phi_m):
    return (
        np.reshape(f_star, (1, 1, nky, nkx)) * phi_m * np.reshape(f, (nky, nkx, 1, 1))
    ).real


def compute_time_average(field):
    "Computes the time average of an N-D field assuming time is the first axis"
    if len(t) == 1:
        time_average = field[0, ...]
    else:
        print("Computing time average...")
        time_average = (
            np.trapz(
                field,
                t,
                axis=0,
            )
            / (t[-1] - t[0])
        )
    return time_average


def write_output():
    print("Writing output...")
    double = np.float64
    if os.path.isfile(output_filename):
        ncfile = nc.Dataset(output_filename, mode="a")
    else:
        ncfile = nc.Dataset(output_filename, mode="w")
        ncfile.createDimension("kx", nkx)
        ncfile.createDimension("ky", nky)
        ncfile.createDimension("kx_prime", nkx)
        ncfile.createDimension("ky_prime", nky)
        kx_nc = ncfile.createVariable("kx", double, ("kx",))
        ky_nc = ncfile.createVariable("ky", double, ("ky",))
        kx_prime = ncfile.createVariable("kx_prime", double, ("kx_prime",))
        ky_prime = ncfile.createVariable("ky_prime", double, ("ky_prime",))
        kx_nc[:] = kx
        ky_nc[:] = ky
        kx_prime[:] = kx
        ky_prime[:] = ky
    T_name = "T_" + tf_type
    T_nc = ncfile.createVariable(
        T_name,
        double,
        (
            "ky_prime",
            "kx_prime",
            "ky",
            "kx",
        ),
    )
    T_nc[:] = T
    T_nc.t_min = t[0]
    T_nc.t_max = t[-1]
    ncfile.close()


if __name__ == "__main__":
    # Parse command line inputs
    input_filename, it_start, it_end, nproc, output_filename, tf_type = parse_args()
    # Open Dataset
    ds = xr.open_dataset(input_filename)
    # Remove un-used variables
    required_vars = ["theta", "kx", "ky", "t", "phi_t"]
    if tf_type == "v":
        is_Tv = True
        print("Computing T_v...")
    elif tf_type == "n":
        is_Tv = False
        required_vars.append("ntot_t")
        print("Computing T_n...")
    elif tf_type == "d":
        is_Tv = False
        required_vars.append("density_t")
        print("Computing T_d...")
    else:
        print(
            "ERROR! tf_type '%s' not recognised. Must be 'v', 'n' or 'd'. Exiting..."
            % tf_type
        )
        sys.exit(1)
    vars_to_delete = ["egrid", "lambda"]  # un-used coords
    for v in ds.data_vars:
        if v not in required_vars:
            vars_to_delete.append(v)
    ds = ds.drop_vars(vars_to_delete)
    if it_end < len(ds["t"]):  # if block to avoid dropping if going right to the end
        ds = ds.drop_sel(t=ds["t"][it_end:])
    ds = ds.drop_sel(t=ds["t"][:it_start])
    # Load data for performance and to avoid concurrent read problems
    ds.load()
    # Pre-compute various quantities for performance
    itheta0 = np.argmin(np.abs(ds["theta"].values))
    kx = np.fft.fftshift(ds["kx"].values)
    nkx = len(kx)
    ky = np.fft.fftshift(np.concatenate((ds["ky"].values, -ds["ky"].values[-1:0:-1])))
    nky = len(ky)
    ikx0 = np.argmin(np.abs(kx))
    iky0 = np.argmin(np.abs(ky))
    # No longer do we have ikyt = iky0  (i.e. target is always zonal flows) as that is a
    # 3D simplification when considering turbulence-zonal transfer only, but here where
    # we calculate the spectrum we are in 4D so this does not apply.
    t = ds["t"].values
    nt = len(t)
    # Pre-compute leading k factors, generalised to 4D
    # k_cross_k_prime_dot_z_hat(ky', kx', ky, kx) = kx * ky' - ky * kx'
    k_cross_k_prime_dot_z_hat = np.reshape(kx, (1, 1, 1, nkx)) * np.reshape(
        ky, (nky, 1, 1, 1)
    ) - np.reshape(ky, (1, 1, nky, 1)) * np.reshape(kx, (1, nkx, 1, 1))
    # k_cross_z_hat_dot_z_hat_cross_k_prime(ky', kx', ky, kx) = -kx * kx' - ky * ky'
    if is_Tv:
        k_cross_z_hat_dot_z_hat_cross_k_prime = -np.reshape(
            kx, (1, 1, 1, nkx)
        ) * np.reshape(kx, (1, nkx, 1, 1)) - np.reshape(
            ky, (1, 1, nky, 1)
        ) * np.reshape(
            ky, (nky, 1, 1, 1)
        )
    # Do calculation
    T = compute_time_average(parallel_compute_transfer())
    print("Applying leading k factors...")
    T *= 2 * k_cross_k_prime_dot_z_hat
    if is_Tv:
        T *= k_cross_z_hat_dot_z_hat_cross_k_prime
    # Write output
    write_output()
    print("Done! Exiting...")
