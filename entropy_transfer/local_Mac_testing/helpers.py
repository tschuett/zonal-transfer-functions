import numpy as np
import xarray as xr

def get_g(nproc,filepath):
    for i in range(nproc):
        ds = xr.open_dataset(filepath+".nc.dfn."+str(i))
        gr = ds["gr"]
        gi = ds["gi"]
        if i == 0:
            dim_dict = {
            "x": ds["ntheta0"].values,
            "y": ds["naky"].values,
            "l": ds["nlambda"].values,
            "e": ds["negrid"].values,
            "s": ds["nspec"].values,
            "nsign": len(ds["sign"]), #already dimensions in original, just carry through
            "ntheta": len(ds["theta"]) #already dimensions in original, just carry through
            }
            gr_all, gi_all = gr.values, gi.values
        else:
            gr_all = np.concatenate((gr_all,gr.values))
            gi_all = np.concatenate((gi_all,gi.values))
            
    layout_reshape_array, reshape_dims_dict = get_layout_reshape_array(ds,dim_dict)
    #print("shape of reshape array: ",layout_reshape_array)

    gr_all = np.reshape(gr_all,layout_reshape_array)
    gi_all = np.reshape(gi_all,layout_reshape_array)
    
    g_all = np.zeros(gr_all.shape, dtype=complex)
    g_all.real = gr_all
    g_all.imag = gi_all
    
    g_all = xr.DataArray(
        data = g_all,
        dims = reshape_dims_dict,
    )
    
    #print(g_all.dims)
    
    #current oder: reshape_dims_dict = flip(layout_string),sign,theta
    #order I want for g: ky,kx,theta,species,energy,lambda,sign
    #use this because np.transpose is broken
    g_all = g_all.transpose("y","x","theta","s","e","l","sign")
    
    #print(g_all.dims)
    
    return g_all

def get_layout_reshape_array(ds,dim_dict):
    layout_string = ds["layout"].values.astype(str).flatten()[0]
    #print("detected layout string: ",layout_string)
    
    #mirror the layout_string as "the rightmost dimensions in layout string are paralleised first" in gs2.
    #The first dimension in the reshape array is the once that splits the entire array up, i.e. the most paralleised dimension
    #given that we obtained the total array by stacking the results from all processors.
    #Hence, we want the rightmost dimension to be in first position in the newshape array
    #as we iterate through and effectively append in for loop below, the flip is needed
    #xy are chose to be on the left since fourier analysis needs access to full domain on each processor
    layout_string = layout_string[::-1] 
    #print("dimesions ordered from left to right: ",layout_string,",sign,theta")
    
    reshape_array = []
    reshape_dims_dict = []
    for i in layout_string:
        reshape_array = np.concatenate((reshape_array,[dim_dict[i]])).astype(int)
        reshape_dims_dict = np.append(reshape_dims_dict,i)
        
    reshape_array = np.concatenate((reshape_array,[dim_dict["nsign"]])).astype(int)
    reshape_array = np.concatenate((reshape_array,[dim_dict["ntheta"]])).astype(int)
    reshape_dims_dict = np.append(reshape_dims_dict,["sign","theta"])
    
    #print(reshape_dims_dict)
    
    return reshape_array, reshape_dims_dict

def ri_to_complex(array_ri):
    array_complex = np.zeros(array_ri.shape[:-1], dtype=complex)
    array_complex.real = array_ri[..., 0]
    array_complex.imag = array_ri[..., 1]
    return array_complex

def move_first_axis_to_last(array_orig):
    # Roll first axis to last place, e.g. for improved cache performance
    # NB: Cannot use np.moveaxis(...) as the return value is just a view of the original
    array_new = np.zeros(
        np.concatenate((array_orig.shape[1:], [array_orig.shape[0]])),
        dtype=array_orig.dtype,
    )
    for i in range(array_orig.shape[0]):
        array_new[..., i] = array_orig[i, ...]
    return array_new

def make_full(array):
    # Assumes dimension order is ky, kx
    # For concatenated array:
    # - ky slice = -1:0:-1 so that we include non-zero kys in reverse order
    # - kx slice = -1::-1 so that we include all kxs in reverse order but this puts kx = 0
    #     to the end even though we still want it at the start, hence use of
    #     np.roll(..., 1, axis=1) so that kx = 0 is at ikx = 0
    # - np.conj(...) of the reversed and rolled array as the negative kys are the conjugate
    #     of the positive kys
    # - np.concatenate(..., axis=0) so that we concatenate in the ky direction
    return np.concatenate(
        (array, np.conj(np.roll(array[-1:0:-1, -1::-1], 1, axis=1))),
        axis=0,
    )