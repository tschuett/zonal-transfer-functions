All this based on equation 16-17 and everything leading up to these equations in the following paper: <br>

M. Nakata, T.-H. Watanabe and H. Sugama, <i>Nonlinear entropy transfer via zonal flows in
gyrokinetic plasma turbulence</i>, Physics of Plasmas 2011, https://aip.scitation.org/doi/full/10.1063/1.3675855