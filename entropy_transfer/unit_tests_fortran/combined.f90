program combined
    use z_hat_module!, only: z_hat_factor
    use maxw_factor_module, only: prefactor
    use phi_mediator, only: get_mediator
    use make_full, only: get_full
    use entropy_transfer, only: compute_entropy_transfer
    use adjust_kxky, only: fftshift, fftshift2D, extend_ky

    implicit none

    integer, parameter :: naky = 2, ntheta0 = 3, nkynew = 3 !kx should be odd, ky can be both
    real, dimension(ntheta0) :: kx = [0,1,-1] !unshifted
    real, dimension(naky) :: ky = [0,1] !unshifted
    real, dimension(nkynew) :: kynew
    real, dimension(nkynew,1,ntheta0) :: z_factor
    integer :: ikx0, iky0, ikyt
    integer :: i,j,k
    
    real :: test_lambdaa, test_energyy, test_bmagg
    real :: maxwellian_factor

    real, dimension(naky,ntheta0) :: phi, g 
    real, dimension(ntheta0) :: g_t
    real, dimension(nkynew,ntheta0,ntheta0) :: phi_m, g_m, T_entropy

    real, dimension(nkynew,ntheta0) :: phi_new, g_new

    call fftshift(kx,ntheta0)
    !write(*,*) kx : PASSED

    call extend_ky(ky,naky,kynew)
    !write(*,*) kynew  : PASSED

    ikx0 = (ntheta0+1)/2
    iky0 = (nkynew+1)/2
    ikyt = iky0

    !write (*,*) ikx0,iky0,ikyt : PASSED

    call z_hat_factor(kx,kynew,z_factor)

    !do i = 1,nkynew
    !    do j=1,ntheta0
    !        write (*,*) z_factor(i,1,j)  : PASSED
    !    end do
    !end do

    test_lambdaa = 2
    test_energyy = 3
    test_bmagg = 4

    call prefactor(test_lambdaa,test_energyy,test_bmagg,maxwellian_factor)
    !write (*,*) maxwellian_factor :PASSED

    !make full phi and g
    phi = reshape( (/ 1,4,2,5,3,6 /) , (/naky,ntheta0/) )
    g = reshape( (/ 7,10,8,11,9,12 /) , (/naky,ntheta0/) )

    !write (*,*) phi

    call get_full(phi,phi_new,ntheta0,naky)
    call get_full(g,g_new,ntheta0,naky)

    !write (*,*) phi_new

    !fftshift phi and g along dim = 1 and dim = 2
    call fftshift2D(phi_new,ntheta0,nkynew)
    call fftshift2D(g_new,ntheta0,nkynew)

    !NO USE OF PHI AND G BELOW, MUST BE PHI_NEW AND G_NEW

    !do i = 1,nkynew
    !    do j=1,ntheta0
    !        write (*,*) phi_new(i,j) !: PASSED
    !   end do
    !end do

    !set g_t with ikyt and g
    g_t = g_new(ikyt,:)

    !get phi mediator:
    call get_mediator(phi_new,ikx0,iky0,ntheta0,nkynew,ikyt,phi_m)

    !do i = 1,nkynew
    !    do j=1,ntheta0
    !        do k=1,ntheta0
    !            write (*,*) phi_m(i,j,k) !: PASSED
    !        end do
    !    end do
    !end do

    !get g mediator:
    call get_mediator(g_new,ikx0,iky0,ntheta0,nkynew,ikyt,g_m)

    !compute entropy transfer
    call compute_entropy_transfer(g_t,g_new, &
    phi_new,g_m,phi_m,z_factor,ntheta0,nkynew,T_entropy)

    do i = 1,nkynew
        do j=1,ntheta0
            do k=1,ntheta0
                write (*,*) T_entropy(i,j,k) !: PASSED
            end do
        end do
    end do
    
    !T_entropy = T_entropy*maxwellian_factor !OMITTED for easier comparison of results, but trivial step


end program combined