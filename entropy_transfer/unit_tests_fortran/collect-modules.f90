module z_hat_module

    implicit none
    private

    public :: z_hat_factor

contains
    subroutine z_hat_factor(kx,ky,result)
        real, dimension(3) :: kx, ky
        integer :: nkx, nky
        real, dimension(3,1) :: kx_new
        real, dimension(1,3) :: ky_new
        real, dimension(3,3) :: result_intermediate
        real, dimension(3,1,3) :: result

        nkx = SIZE(kx)
        nky = SIZE(ky)

        kx_new = reshape( kx, (/ 3, 1 /) )
        ky_new = reshape( ky, (/ 1, 3 /) )

        result_intermediate = matmul(kx_new,ky_new)

        result = reshape( result_intermediate, (/ 3, 1, 3 /) )

    end subroutine z_hat_factor

    

end module z_hat_module

module maxw_factor_module
    implicit none
    private

    public :: prefactor

contains
    subroutine prefactor(lambdaa,energyy,bmagg,result)
        real :: lambdaa, energyy, bmagg, result, c
        REAL, PARAMETER :: PI = 3.1415927

        c = ((2*PI)**(1.5))/(2*bmagg)

        result = c*EXP(energyy*(1+bmagg/(2*lambdaa)))

    end subroutine prefactor

end module maxw_factor_module

module phi_mediator
    implicit none
    private

    public :: get_mediator

contains
    subroutine get_mediator(field,ikx0,iky0,nkx,nky,ikyt,result)
        integer :: ikx0, iky0, nkx, nky, ikyt
        real, dimension(nky,nkx) :: field
        real, dimension(nky,nkx,nkx) :: result
        integer :: ikys, ikxs, ikxt, ikxm, ikym

        do ikys = 1, nky
            do ikxs = 1, nkx
                do ikxt = 1, nkx
                    ikxm = ikxt - ikxs + ikx0
                    ikym = ikyt - ikys + iky0

                    !changes due to 1-indexing vs 0-indexing in python
                    if (.not.((1 .le. ikxm) .and. (ikxm .le. nkx) .and. (1 .le. ikym) .and. (ikym .le. nky))) then 
                        result(ikys,ikxs,ikxt) = 0.0
                        CYCLE
                    end if

                    result(ikys,ikxs,ikxt) = field(ikym,ikxm) !switch because of coloumn-row vs r-c in python

                end do
            end do
        end do

    end subroutine get_mediator

end module phi_mediator


module adjust_kxky

    implicit none
    private

    public :: fftshift, fftshift2D, extend_ky

contains
    subroutine fftshift(kx,ntheta0)
        integer :: ntheta0, shift_no
        real, dimension(ntheta0) :: kx

        shift_no = -(ntheta0-1)/2
        kx = cshift(kx, SHIFT=shift_no, DIM=1)

    end subroutine fftshift

    subroutine fftshift2D(field_array,ntheta0,nkynew)
        integer :: ntheta0, nkynew, shift_no
        real, dimension(nkynew,ntheta0) :: field_array

        shift_no = -(nkynew-1)/2
        field_array = cshift(field_array, SHIFT=shift_no, DIM=1)

        shift_no = -(ntheta0-1)/2
        field_array = cshift(field_array, SHIFT=shift_no, DIM=2)

    end subroutine fftshift2D

    subroutine extend_ky(ky,naky,kynew)
        integer :: naky, ky0, nkynew
        real, dimension(naky) :: ky
        real, dimension(2*naky-1) :: kynew
        integer :: i

        nkynew = 2*naky-1

        ky0 = naky
        kynew(naky:) = ky
        
        do i=1, naky-1
            kynew(i) = -ky(naky+1-i)
        end do


    end subroutine extend_ky

end module adjust_kxky

module make_full
    implicit none
    private

    public :: get_full

contains
    subroutine get_full(field_arr,result,nkx,nky)
        integer :: nkx, nky
        real, dimension(nky,nkx) :: field_arr
        real, dimension(nky-1,nkx) :: temp, temp2
        real, dimension(2*nky-1,nkx) :: result
        integer :: i

        result(1:nky,:) = field_arr
        temp = field_arr(2:,:) 

        !reverse order of temp along x
        do i = 1, nkx
            temp2(:,nkx+1-i) = temp(:,i)
        end do

        !write (*,*) temp2

        ! equivalent to np.roll to have ikx0 as first index to be "fft-unshifted"
        temp2 = cshift(temp2, SHIFT=-1,DIM=2)

        result(nky+1:,:) = temp2

    end subroutine get_full

end module make_full

module entropy_transfer

    implicit none
    private

    public :: compute_entropy_transfer

contains
    subroutine compute_entropy_transfer(gt,gs,phi,gm,phim,z_hat_factor,nkx,nky,result)
        integer :: nkx, nky
        real, dimension(nky,1,nkx):: z_hat_factor
        real, dimension(nkx) :: gt
        real, dimension(1,1,nkx) :: gt_new
        real, dimension(nky,nkx) :: gs, phi
        real, dimension(nky,nkx,1) :: gs_new, phi_new
        real, dimension(nky,1,nkx) :: phim, gm
        real, dimension(nky,nkx,nkx) :: result
        integer :: i,j,k

        gt_new = reshape(gt,(/1,1,nkx/))
        gs_new = reshape(gs,(/nky,nkx,1/))
        phi_new = reshape(phi,(/nky,nkx,1/))

        do i=1,nky
            do j=1,nkx
                do k=1,nkx
                    result(i,j,k) = (gt_new(1,1,k)*phim(i,j,k)*gs_new(i,j,1) &
                                    - gt_new(1,1,k)*gm(i,j,k)*phi_new(i,j,1)) &
                                    *z_hat_factor(i,1,k)
                end do
            end do
        end do

    end subroutine compute_entropy_transfer

end module entropy_transfer