module entropy_transfer

    implicit none
    private

    public :: compute_entropy_transfer

contains
    subroutine compute_entropy_transfer(gt,gs,phi,gm,phim,z_hat_factor,nkx,nky,result)
        integer :: nkx, nky
        integer :: z_hat_factor
        integer, dimension(nkx) :: gt
        integer, dimension(1,1,nkx) :: gt_new
        integer, dimension(nky,nkx) :: gs, phi
        integer, dimension(nky,nkx,1) :: gs_new, phi_new
        integer, dimension(nky,1,nkx) :: phim, gm
        integer, dimension(nky,nkx,nkx) :: result
        integer :: i,j,k

        gt_new = reshape(gt,(/1,1,nkx/))
        gs_new = reshape(gs,(/nky,nkx,1/))
        phi_new = reshape(phi,(/nky,nkx,1/))

        do i=1,nky
            do j=1,nkx
                do k=1,nkx
                    result(i,j,k) = (gt_new(1,1,k)*phim(i,j,k)*gs_new(i,j,1) &
                                    - gt_new(1,1,k)*gm(i,j,k)*phi_new(i,j,1))*z_hat_factor
                end do
            end do
        end do

    end subroutine compute_entropy_transfer

end module entropy_transfer

program test
    use entropy_transfer, only: compute_entropy_transfer

    implicit none

    integer, parameter :: test_nkx = 2, test_nky = 2
    integer :: test_z_hat_factor = 5
    integer, dimension(test_nkx) :: test_gt = [1,2]
    integer, dimension(test_nky,test_nkx) :: test_gs, test_phi
    integer, dimension(test_nky,test_nkx,test_nkx) :: test_phim, test_gm 
    integer, dimension(test_nky,test_nkx,test_nkx) :: test_result
    integer :: i,j,k

    test_gs = reshape( (/ 1,1,2,2 /) , (/2,2/) )
    test_phi = reshape( (/ 1,1,2,2 /) , (/2,2/) )

    test_gm = reshape( (/ 4,4,4,4,5,5,5,5 /) , (/2,2,2/) )
    test_phim = reshape( (/ 1,1,1,1,2,2,2,2 /) , (/2,2,2/) )

    call compute_entropy_transfer(test_gt,test_gs, &
    test_phi,test_gm,test_phim,test_z_hat_factor,test_nkx,test_nky,test_result)

    do i=1,test_nky
        do j=1,test_nkx
            !write (*,*) test_gs(i,j)
            do k=1,test_nkx
                !write (*,*) test_gm(i,j,k)
                write (*,*) test_result(i,j,k)
            end do
        end do
    end do
    !!! TEST PASSED, agrees with python


end program test