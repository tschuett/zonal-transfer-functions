module z_hat_module

    implicit none
    private

    public :: z_hat_factor

contains
    subroutine z_hat_factor(kx,ky,result)
        real, dimension(3) :: kx, ky
        integer :: nkx, nky
        real, dimension(3,1) :: kx_new
        real, dimension(1,3) :: ky_new
        real, dimension(3,3) :: result_intermediate
        real, dimension(3,1,3) :: result

        nkx = SIZE(kx)
        nky = SIZE(ky)

        kx_new = reshape( kx, (/ 3, 1 /) )
        ky_new = reshape( ky, (/ 1, 3 /) )

        result_intermediate = matmul(kx_new,ky_new)

        result = reshape( result_intermediate, (/ 3, 1, 3 /) )

    end subroutine z_hat_factor

    

end module z_hat_module

program test
    use z_hat_module, only: z_hat_factor
    implicit none

    real, dimension(3) :: test_kx, test_ky
    real, dimension(3,1,3) :: test_result !nkx,1,nky flipped w.r.t python
    integer :: i,j
    

    test_kx = [-1,0,1]
    test_ky = [-3,0,1]

    call z_hat_factor(test_kx,test_ky,test_result)

    do i=1,3
        do j=1,3
            !bc of row-coloumn asymmetry this should correspond to python output
            write (*,*) test_result(j,1,i) 
        end do
    end do
    !!! TEST PASSED, agrees with python
    
end program test