module make_full
    implicit none
    private

    public :: get_full

contains
    subroutine get_full(field_arr,result,nkx,nky)
        integer :: nkx, nky
        real, dimension(nky,nkx) :: field_arr
        real, dimension(nky-1,nkx) :: temp, temp2
        real, dimension(2*nky-1,nkx) :: result
        integer :: i

        result(1:nky,:) = field_arr
        temp = field_arr(2:,:) 

        !reverse order of temp along x
        do i = 1, nkx
            temp2(:,nkx+1-i) = temp(:,i)
        end do

        !write (*,*) temp2

        ! equivalent to np.roll to have ikx0 as first index to be "fft-unshifted"
        temp2 = cshift(temp2, SHIFT=-1,DIM=2)

        result(nky+1:,:) = temp2

    end subroutine get_full

end module make_full

program test
    use make_full, only: get_full
    implicit none

    integer, parameter :: naky = 2, ntheta0 = 3, nkynew = 3
    real, dimension(naky,ntheta0) :: test_array
    real, dimension(nkynew,ntheta0) :: test_result
    integer :: i, j

    test_array = reshape( (/ 1,4,2,5,3,6 /) , (/naky,ntheta0/) ) !this way same indexing as python array

    !length of test array in 2nd row dimension is kx and ky and becomes
    ! kx and kynew in result to extend phi to negative ky values

    call get_full(test_array,test_result,ntheta0,naky)

    do i=1, nkynew
        do j=1, ntheta0
            write (*,*) test_result(i,j) !swich bc of C-R vs R-C in python
        end do
    end do
    !!! TEST PASSED, agrees with python

end program test