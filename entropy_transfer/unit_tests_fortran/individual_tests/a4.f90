module phi_mediator
    implicit none
    private

    public :: get_phi_mediator

contains
    subroutine get_phi_mediator(phi,ikx0,iky0,nkx,nky,ikyt,result)
        real, dimension(nky,nkx) :: phi
        integer :: ikx0, iky0, nkx, nky, ikyt
        real, dimension(nky,nkx,nkx) :: result
        integer :: ikys, ikxs, ikxt, ikxm, ikym

        do ikys = 1, nky
            do ikxs = 1, nkx
                do ikxt = 1, nkx
                    ikxm = ikxt - ikxs + ikx0
                    ikym = ikyt - ikys + iky0

                    !changes due to 1-indexing vs 0-indexing in python
                    if (.not.((1 .le. ikxm) .and. (ikxm .le. nkx) .and. (1 .le. ikym) .and. (ikym .le. nky))) then 
                        result(ikys,ikxs,ikxt) = 0.0
                        CYCLE
                    end if

                    result(ikys,ikxs,ikxt) = phi(ikym,ikxm) !switch because of coloumn-row vs r-c in python

                end do
            end do
        end do

    end subroutine get_phi_mediator

end module phi_mediator

program test
    use phi_mediator, only: get_phi_mediator
    implicit none

    real, dimension(3,2) :: test_phi
    integer :: test_ikx0, test_iky0, test_nkx, test_nky, test_ikyt
    real, dimension(3,2,2) :: test_result
    integer :: i,j,k

    test_phi = reshape( (/ 1,3,3,2,4,4 /) , (/3,2/) )
    test_ikx0 = 1+1
    test_iky0 = 1+1
    test_nkx = 2
    test_nky = 3
    test_ikyt = test_iky0

    call get_phi_mediator(test_phi,test_ikx0,test_iky0,test_nkx,test_nky,test_ikyt,test_result)

    do i=1,test_nky
        do j=1, test_nkx
            do k=1, test_nkx
                write (*,*) test_result(i,j,k)
            end do
        end do
    end do
    !!! TEST PASSED, agrees with python

end program test

