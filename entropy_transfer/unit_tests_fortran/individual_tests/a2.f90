module maxw_factor_module
    implicit none
    private

    public :: prefactor

contains
    subroutine prefactor(lambdaa,energyy,bmagg,result)
        real :: lambdaa, energyy, bmagg, result, c
        REAL, PARAMETER :: PI = 3.1415927

        c = ((2*PI)**(1.5))/(2*bmagg)

        result = c*EXP(energyy*(1+bmagg/(2*lambdaa)))

    end subroutine prefactor

end module maxw_factor_module

program test
    use maxw_factor_module, only: prefactor
    implicit none

    real :: test_lambdaa, test_energyy, test_bmagg
    real :: test_result

    test_lambdaa = 2
    test_energyy = 3
    test_bmagg = 4

    call prefactor(test_lambdaa,test_energyy,test_bmagg,test_result)

    write (*,*) test_result
    !!! TEST PASSED, agrees with python
    


end program test