#! /usr/bin/env python3
"""
Script to compute nonlinear transfer function

Copyright 2020 Steve Biggs

This file is part of initial.

initial is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

initial is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with initial.  If not, see http://www.gnu.org/licenses/.
"""

import multiprocessing as mp
import numpy as np
import sys
import warnings
import xarray as xr


def parse_args():
    input_filename, it_start, it_end, nproc, output_filename = sys.argv[1:]
    it_start = int(it_start)
    it_end = int(it_end)
    nproc = int(nproc)
    return input_filename, it_start, it_end, nproc, output_filename


def parallel_compute_transfer_along_theta():
    # Loop over theta in parallel
    with mp.Pool(processes=nproc) as p:
        results = np.array(
            p.map(parallel_compute_transfer_wrapper, range(ntheta), ntheta // nproc),
            dtype=float,
        )
    return results


def parallel_compute_transfer_wrapper(i_theta):
    print("Computing net transfer for theta = {}".format(theta[i_theta]))
    return compute_net_transfer_this_theta(
        ntot_t[:, 0, :, :, i_theta, :],
        density_t[:, 0, :, :, i_theta, :],
        phi_t[:, :, :, i_theta, :],
        ntot_err[:, 0, :, :, i_theta, :],
        density_err[:, 0, :, :, i_theta, :],
        phi_err[:, :, :, i_theta, :],
    )


def compute_net_transfer_this_theta(ntot, density, phi, n_err, d_err, p_err):
    # No complex number support in uncertainties package so have to deal with real and
    # imag parts manually. Initial re-ordering operations, etc. should work OK. Conj is
    # easy. It's just multiplication that gets tricky.
    # Move t axis to last position for improved cache performance
    ntot = move_first_axis_to_last(ntot)
    density = move_first_axis_to_last(density)
    phi = move_first_axis_to_last(phi)
    n_err = move_first_axis_to_last(n_err)
    d_err = move_first_axis_to_last(d_err)
    p_err = move_first_axis_to_last(p_err)
    # Make full versions of arrays to simplify indexing
    ntot = make_full(ntot)
    density = make_full(density)
    phi = make_full(phi)
    n_err = make_full(n_err)
    d_err = make_full(d_err)
    p_err = make_full(p_err)
    # Pre-shift arrays so that indexing works correctly
    ntot = np.fft.fftshift(ntot, axes=(0, 1))
    density = np.fft.fftshift(density, axes=(0, 1))
    phi = np.fft.fftshift(phi, axes=(0, 1))
    n_err = np.fft.fftshift(n_err, axes=(0, 1))
    d_err = np.fft.fftshift(d_err, axes=(0, 1))
    p_err = np.fft.fftshift(p_err, axes=(0, 1))
    # Pre-compute complex conjugate array
    conj_shape = (1, 1, nkx, nt, 2)  # target shape
    ntot_star = np.reshape(conj(ntot[ikyt, ...]), conj_shape)
    density_star = np.reshape(conj(density[ikyt, ...]), conj_shape)
    phi_star = np.reshape(conj(phi[ikyt, ...]), conj_shape)
    ntot_star_err = np.reshape(n_err[ikyt, ...], conj_shape)
    density_star_err = np.reshape(d_err[ikyt, ...], conj_shape)
    phi_star_err = np.reshape(p_err[ikyt, ...], conj_shape)
    # Pre-prepare array of mediators for performance
    phi_m, phi_m_err = compute_phi_m(phi, p_err, ikx0, iky0)
    # Reshape phi etc for readability later
    source_shape = (nky, nkx, 1, nt, 2)
    ntot = np.reshape(ntot, source_shape)
    density = np.reshape(density, source_shape)
    phi = np.reshape(phi, source_shape)
    n_err = np.reshape(n_err, source_shape)
    d_err = np.reshape(d_err, source_shape)
    p_err = np.reshape(p_err, source_shape)
    # Compute transfer and return
    return compute_net_transfer(
        ntot,
        density,
        phi,
        ntot_star,
        density_star,
        phi_star,
        phi_m,
        n_err,
        d_err,
        p_err,
        ntot_star_err,
        density_star_err,
        phi_star_err,
        phi_m_err,
    )


def move_first_axis_to_last(array_orig):
    # Roll first axis to last place, e.g. for improved cache performance
    # NB: Cannot use np.moveaxis(...) as the return value is just a view of the original
    # Actually, t goes second to last as ri dimension is last as we don't have complex
    # number support in uncertainties package
    array_new = np.zeros(
        np.concatenate(
            (array_orig.shape[1:-1], [array_orig.shape[0]], [array_orig.shape[-1]])
        ),
        dtype=array_orig.dtype,
    )
    for i in range(array_orig.shape[0]):
        array_new[..., i, :] = array_orig[i, ...]
    return array_new


def make_full(array):
    # Assumes dimension order is ky, kx, t, ri
    # For concatenated array:
    # - ky slice = -1:0:-1 so that we include non-zero kys in reverse order
    # - kx slice = -1::-1 so that we include all kxs in reverse order but this puts kx = 0
    #     to the end even though we still want it at the start, hence use of
    #     np.roll(..., 1, axis=1) so that kx = 0 is at ikx = 0
    # - conj(...) of the reversed and rolled array as the negative kys are the conjugate
    #     of the positive kys
    # - np.concatenate(..., axis=0) so that we concatenate in the ky direction
    return np.concatenate(
        (array, conj(np.roll(array[-1:0:-1, -1::-1, ...], 1, axis=1))),
        axis=0,
    )


def conj(arr):
    # Assumes ri is last dimension
    shape = np.ones(len(arr.shape), dtype=int)
    shape[-1] = 2
    return np.reshape(np.array([1, -1]), shape) * arr


def compute_phi_m(phi, p_err, ikx0, iky0):
    # Pre-allocate array
    # NB: Cannot pre-allocate this variable in the global scope and re-use it here
    phi_m = np.zeros((nky, nkx, nkx, nt, 2))
    phi_m_err = np.zeros((nky, nkx, nkx, nt, 2))
    # Loop over target and source wavenumbers
    for ikys in range(nky):
        for ikxs in range(nkx):
            for ikxt in range(nkx):
                # Work out index of mediator
                ikxm = ikxt - ikxs + ikx0
                ikym = ikyt - ikys + iky0
                # Check mediator index exists
                if not (0 <= ikxm and ikxm < nkx and 0 <= ikym and ikym < nky):
                    # Just don't set a value to avoid unnecessary cache misses
                    continue
                # Store mediator value in mediator array
                phi_m[ikys, ikxs, ikxt, ...] = phi[ikym, ikxm, ...]
                phi_m_err[ikys, ikxs, ikxt, ...] = p_err[ikym, ikxm, ...]
    # Return output array
    return phi_m, phi_m_err


def compute_net_transfer(
    ntot,
    density,
    phi,
    ntot_star,
    density_star,
    phi_star,
    phi_m,
    n_err,
    d_err,
    p_err,
    ntot_star_err,
    density_star_err,
    phi_star_err,
    phi_m_err,
):
    # T_v
    phi_star_times_phi_m = complex_product(phi_star, phi_m)
    phi_star_times_phi_m_err = complex_product_err(
        phi_star, phi_m, phi_star_times_phi_m, phi_star_err, phi_m_err
    )
    triple_product = complex_product(phi_star_times_phi_m, phi)
    try:
        triple_product_err = complex_product_err(
            phi_star_times_phi_m, phi, triple_product, phi_star_times_phi_m_err, p_err
        )
    except ZeroDivisionError as e:
        print("phi_star =", end="")
        print(phi_star[0, 0, 0, 0, :])
        print("phi_m =", end="")
        print(phi_m[42, 21, 0, 0, :])
        raise (e)
    del phi_star_times_phi_m, phi_star_times_phi_m_err
    # Ignore time average as only calculating at one time point
    T_v = 2 * z_hat_dot_k_cross_k_prime * k_factor_for_T_v * triple_product[..., 0]
    T_v_err = T_v * np.sqrt(
        z_hat_dot_k_cross_k_prime_rel_err2
        + k_factor_for_T_v_rel_err2
        + safe_divide(triple_product_err[..., 0], triple_product[..., 0]) ** 2
    )

    # T_n
    ntot_star_times_phi_m = complex_product(ntot_star, phi_m)
    ntot_star_times_phi_m_err = complex_product_err(
        ntot_star, phi_m, ntot_star_times_phi_m, ntot_star_err, phi_m_err
    )
    triple_product = complex_product(ntot_star_times_phi_m, ntot)
    triple_product_err = complex_product_err(
        ntot_star_times_phi_m, ntot, triple_product, ntot_star_times_phi_m_err, n_err
    )
    del ntot_star_times_phi_m, ntot_star_times_phi_m_err
    T_n = 2 * z_hat_dot_k_cross_k_prime * triple_product[..., 0]
    T_n_err = T_n * np.sqrt(
        z_hat_dot_k_cross_k_prime_rel_err2
        + safe_divide(triple_product_err[..., 0], triple_product[..., 0]) ** 2
    )

    # T_d
    density_star_times_phi_m = complex_product(density_star, phi_m)
    density_star_times_phi_m_err = complex_product_err(
        density_star, phi_m, density_star_times_phi_m, density_star_err, phi_m_err
    )
    triple_product = complex_product(density_star_times_phi_m, density)
    triple_product_err = complex_product_err(
        density_star_times_phi_m,
        density,
        triple_product,
        density_star_times_phi_m_err,
        d_err,
    )
    del density_star_times_phi_m, density_star_times_phi_m_err
    T_d = 2 * z_hat_dot_k_cross_k_prime * triple_product[..., 0]
    T_d_err = T_d * np.sqrt(
        z_hat_dot_k_cross_k_prime_rel_err2
        + safe_divide(triple_product_err[..., 0], triple_product[..., 0]) ** 2
    )
    del triple_product, triple_product_err

    return (
        np.sum(T_v),
        np.sum(T_v_err ** 2),
        np.sum(T_n),
        np.sum(T_n_err ** 2),
        np.sum(T_d),
        np.sum(T_d_err ** 2),
    )


def complex_product(a, b):
    # Assumes ri is last dimension
    shape = np.zeros(len(a.shape), dtype=int)
    for i in range(len(a.shape)):
        shape[i] = np.max([a.shape[i], b.shape[i]])
    c = np.zeros(shape)
    c[..., 0] = a[..., 0] * b[..., 0] - a[..., 1] * b[..., 1]  # real
    c[..., 1] = a[..., 0] * b[..., 1] + a[..., 1] * b[..., 0]  # imag
    return c


def complex_product_err(a, b, c, a_err, b_err):
    c_err = np.zeros(c.shape)
    c_err[..., 0] = np.sqrt(
        (
            a[..., 0]
            * b[..., 0]
            * np.sqrt(
                safe_divide(a_err[..., 0], a[..., 0]) ** 2
                + safe_divide(b_err[..., 0], b[..., 0]) ** 2
            )
        )
        ** 2
        + (
            -a[..., 1]  # minus probably doesn't matter
            * b[..., 1]
            * np.sqrt(
                safe_divide(a_err[..., 1], a[..., 1]) ** 2
                + safe_divide(b_err[..., 1], b[..., 1]) ** 2
            )
        )
        ** 2
    )
    c_err[..., 1] = np.sqrt(
        (
            a[..., 0]
            * b[..., 1]
            * np.sqrt(
                safe_divide(a_err[..., 0], a[..., 0]) ** 2
                + safe_divide(b_err[..., 1], b[..., 1]) ** 2
            )
        )
        ** 2
        + (
            a[..., 1]
            * b[..., 0]
            * np.sqrt(
                safe_divide(a_err[..., 1], a[..., 1]) ** 2
                + safe_divide(b_err[..., 0], b[..., 0]) ** 2
            )
        )
        ** 2
    )
    return np.where(c == 0, 0, c_err)  # avoid non-zero error on zero value


def compute_time_average(field):
    "Computes the time average of an N-D field assuming time is the last axis"
    if len(t) == 1:
        time_average = field[..., 0]
    else:
        time_average = (
            np.trapz(
                field,
                t,
            )
            / (t[-1] - t[0])
        )
    return time_average


def write_output():
    with open(output_filename, "w") as f:
        f.write(
            "{:6s}, {:11s}, {:11s}, {:11s}, {:11s}, {:11s}, {:11s}\n".format(
                "theta", "T_v", "T_v_err", "T_n", "T_n_err", "T_d", "T_d_err"
            )
        )
        for i in range(ntheta):
            f.write(
                "{:6.3f}, {:11.4e}, {:11.4e}, {:11.4e}, {:11.4e}, {:11.4e}, {:11.4e}\n".format(
                    theta[i], *results[i, :]
                )
            )


def estimate_uncertainty(arr):
    with warnings.catch_warnings():
        # ignore np.log2(0) divide by zero warning
        warnings.simplefilter("ignore", category=RuntimeWarning)
        return 0.5 * 2 ** (np.floor(np.log2(np.abs(arr))) - 52)


def get_index(i, arr):
    shape = np.array(arr.shape)
    # shape_ext holds virtual extra dimension of length 1 to allow index calculation
    # below to generalise for all dimensions
    shape_ext = np.concatenate((shape, [1]))
    index = len(shape) * [0]  # list to support item assignment
    index = [i // np.prod(shape_ext[n + 1 :]) % shape[n] for n in range(len(shape))]
    return tuple(index)  # return tuple so it can be used like array[index]


def safe_divide_debug(a, b):
    assert a.shape == b.shape
    assert a.dtype == b.dtype
    c = np.zeros_like(a)
    for i in range(len(a.flatten())):
        index = get_index(i, a)
        if a[index] == 0 and b[index] == 0:
            # print("WARNING! a = b = 0 at index " + str(index))
            c[index] = 0
        elif a[index] != 0 and b[index] == 0:
            print(
                "ERROR! a["
                + str(index)
                + "] = "
                + str(a[index])
                + " != 0 but b = "
                + str(b[index])
                + " = 0"
            )
            raise (ZeroDivisionError)
        else:
            c[index] = a[index] / b[index]
    return c
    # try:
    #    with np.errstate(invalid="ignore", divide="raise"):
    #        return np.where(np.logical_and(a == 0, b == 0), 0, a / b)
    # except FloatingPointError as e:
    #    print("FloatingPointError: divide by zero in a / b at elements:")
    #    for i in range(len(a.flatten())):
    #        if a.flatten()[i] != 0 and b.flatten()[i] == 0:
    #            print("i = {}, a[i] = {}, b[i] = {}".format(i, a.flatten()[i],
    #                b.flatten()[i]))


def safe_divide(a, b):
    with np.errstate(invalid="ignore", divide="raise"):
        return np.where(np.logical_and(a == 0, b == 0), 0, a / b)


if __name__ == "__main__":
    # Parse command line inputs
    input_filename, it_start, it_end, nproc, output_filename = parse_args()
    # Open Dataset
    if "," in input_filename:
        input_filename_list = [f for f in input_filename.split(",")]
        ds = xr.open_mfdataset(input_filename_list)
    else:
        ds = xr.open_dataset(input_filename)
    # Remove un-used variables
    required_vars = ["theta", "kx", "ky", "t", "density_t", "ntot_t", "phi_t"]
    vars_to_delete = ["egrid", "lambda"]  # un-used coords
    for v in ds.data_vars:
        if v not in required_vars:
            vars_to_delete.append(v)
    ds = ds.drop_vars(vars_to_delete)
    ds = ds.drop_sel(t=ds["t"][it_end:])
    ds = ds.drop_sel(t=ds["t"][:it_start])
    # Load data for performance and to avoid concurrent read problems
    ds.load()
    # Extract named variables
    theta = ds["theta"].values
    kx = ds["kx"].values
    kx = np.fft.fftshift(kx)
    ky = ds["ky"].values
    ky = np.fft.fftshift(np.concatenate((ky, -ky[-1:0:-1])))
    t = ds["t"].values
    density_t = ds["density_t"].values
    ntot_t = ds["ntot_t"].values
    phi_t = ds["phi_t"].values
    # Estimate uncertainty as numerical precision
    kx_err = estimate_uncertainty(kx)
    print("kx =")
    print(kx)
    print("kx_err =")
    print(kx_err)
    ky_err = estimate_uncertainty(ky)
    print("ky =")
    print(ky)
    print("ky_err =")
    print(ky_err)
    density_err = estimate_uncertainty(density_t)
    ntot_err = estimate_uncertainty(ntot_t)
    phi_err = estimate_uncertainty(phi_t)
    # Pre-compute various quantities for performance
    ntheta = len(theta)
    nkx = len(kx)
    nky = len(ky)
    ikx0 = np.argmin(np.abs(kx))
    iky0 = np.argmin(np.abs(ky))
    ikyt = iky0  # i.e. target is always zonal flows
    nt = len(t)
    # Reshape kx etc for readability
    #                 ky',kx',kx,t
    ky_prime_shape = (nky, 1, 1, 1)
    kx_prime_shape = (1, nkx, 1, 1)
    kx_shape = (1, 1, nkx, 1)
    ky_prime = np.reshape(ky, ky_prime_shape)
    kx_prime = np.reshape(kx, kx_prime_shape)
    kx = np.reshape(kx, kx_shape)
    ky_prime_err = np.reshape(ky_err, ky_prime_shape)
    kx_prime_err = np.reshape(kx_err, kx_prime_shape)
    kx_err = np.reshape(kx_err, kx_shape)
    # Pre-compute k-factors for performance
    z_hat_dot_k_cross_k_prime = kx * ky_prime
    z_hat_dot_k_cross_k_prime_rel_err2 = (
        safe_divide(kx_err, kx) ** 2 + safe_divide(ky_prime_err, ky_prime) ** 2
    )
    k_factor_for_T_v_shape = (1, nkx, nkx, 1)
    k_factor_for_T_v = -kx * kx_prime
    k_factor_for_T_v_rel_err2 = (
        safe_divide(kx_err, kx) ** 2 + safe_divide(kx_prime_err, kx_prime) ** 2
    )
    # Do calculation
    results = parallel_compute_transfer_along_theta()
    # Write output
    write_output()
